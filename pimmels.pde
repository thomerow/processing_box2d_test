import shiffman.box2d.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;

PImage _pimmelTexture;
static final float PimmelSize = 96.0;
ArrayList<Pimmel> _pimmels;
Box2DProcessing _box2d;
Boundary _bottom, _left, _right;

void settings() {
  size(1600, 900, P2D);
}

void setup() {
  //surface.setResizable(true);
  orientation(LANDSCAPE);

  _box2d = new Box2DProcessing(this, 100.0);
  _box2d.createWorld(); 
  _box2d.setGravity(0, -9.8);  // Set earthlike gravity
 
  _pimmels = new ArrayList<Pimmel>();
  _bottom = new Boundary(width / 2, height - 50, (width / 10) * 6, 2);
  _left = new Boundary((width / 10) * 2, (height / 10) * 9 - 50, 2, (height / 10) * 2);
  _right = new Boundary((width / 10) * 8, (height / 10) * 9 - 50, 2, (height / 10) * 2);

  _pimmelTexture = loadImage("pimmel.png");

  // Writing to the depth buffer is disabled to avoid rendering
  // artifacts due to the fact that the character textures are semi-transparent
  // but not z-sorted.
  hint(DISABLE_DEPTH_MASK);
}

void draw() {
  background(255);
  
  _box2d.step();
  
  // TEST:
  if (mousePressed) {
    switch (mouseButton) {
      case LEFT:
        Pimmel p = new Pimmel(mouseX, mouseY);
        p.setLinearVelocity(new Vec2(random(-1, 1), random(-5, -2)));
        p.setAngularVelocity(random(-5, 5));
        _pimmels.add(p);
        break;
        
      case RIGHT:
        for (Pimmel pKill: _pimmels) {
          pKill.killBody();          
        }
        _pimmels.clear();
        break;
    }
  }

  for (int i = _pimmels.size() - 1; i >= 0; --i) {
    Pimmel p = _pimmels.get(i);
    if (p.destroyIfDone()) {
      _pimmels.remove(i);
    }
    else p.draw();
  }
  
  _bottom.draw();
  _left.draw();
  _right.draw();
  fill(0);
  text("Pimmel: " + _pimmels.size(), 10, 20);
}