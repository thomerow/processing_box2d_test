// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2012
// Box2DProcessing example

// A fixed boundary class

class Boundary {

  // A boundary is a simple rectangle with x,y,width,and height
  float _x;
  float _y;
  float _w;
  float _h;
  
  // But we also have to make a body for box2d to know about it
  Body b;

  Boundary(float x, float y, float w, float h) {
    _x = x;
    _y = y;
    _w = w;
    _h = h;

    // Define the polygon
    PolygonShape sd = new PolygonShape();
    // Figure out the box2d coordinates
    float box2dW = _box2d.scalarPixelsToWorld(w / 2);
    float box2dH = _box2d.scalarPixelsToWorld(h / 2);
    // We're just a box
    sd.setAsBox(box2dW, box2dH);


    // Create the body
    BodyDef bd = new BodyDef();
    bd.type = BodyType.STATIC;
    bd.position.set(_box2d.coordPixelsToWorld(x, y));
    b = _box2d.createBody(bd);
    
    // Attached the shape to the body using a Fixture
    b.createFixture(sd, 1);
    
    b.setUserData(this);
  }

  // Draw the boundary, if it were at an angle we'd have to do something fancier
  void draw() {
    fill(0);
    stroke(0);
    rectMode(CENTER);
    rect(_x, _y, _w, _h);
  }
}