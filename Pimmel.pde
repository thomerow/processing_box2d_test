// A schlong flying around on the screen
class Pimmel {
  PShape _s;
  Body _body;
        
  Pimmel(float x, float y) {
    
    // Build Body
    createBody(x, y);

    _s = createPimmelShape();
  }
      
  void createBody(float x, float y) {
    BodyDef bd = new BodyDef();      
    bd.type = BodyType.DYNAMIC;
    bd.position.set(_box2d.coordPixelsToWorld(x, y));
    _body = _box2d.createBody(bd);
    
    PolygonShape ps = new PolygonShape();
    
    Vec2[] vertices = new Vec2[8];
    vertices[0] = _box2d.vectorPixelsToWorld(new Vec2(0.40 * PimmelSize - (PimmelSize / 2), 0.02 * PimmelSize - (PimmelSize / 2)));
    vertices[1] = _box2d.vectorPixelsToWorld(new Vec2(0.60 * PimmelSize - (PimmelSize / 2), 0.02 * PimmelSize - (PimmelSize / 2)));
    vertices[2] = _box2d.vectorPixelsToWorld(new Vec2(0.60 * PimmelSize - (PimmelSize / 2), 0.60 * PimmelSize - (PimmelSize / 2)));
    vertices[3] = _box2d.vectorPixelsToWorld(new Vec2(0.76 * PimmelSize - (PimmelSize / 2), 0.60 * PimmelSize - (PimmelSize / 2)));
    vertices[4] = _box2d.vectorPixelsToWorld(new Vec2(0.76 * PimmelSize - (PimmelSize / 2), 0.96 * PimmelSize - (PimmelSize / 2)));
    vertices[5] = _box2d.vectorPixelsToWorld(new Vec2(0.24 * PimmelSize - (PimmelSize / 2), 0.96 * PimmelSize - (PimmelSize / 2)));
    vertices[6] = _box2d.vectorPixelsToWorld(new Vec2(0.24 * PimmelSize - (PimmelSize / 2), 0.60 * PimmelSize - (PimmelSize / 2)));
    vertices[7] = _box2d.vectorPixelsToWorld(new Vec2(0.40 * PimmelSize - (PimmelSize / 2), 0.60 * PimmelSize - (PimmelSize / 2)));
    
    ps.set(vertices, vertices.length);
    
    // Define a fixture
    FixtureDef fd = new FixtureDef();
    fd.shape = ps;    
    // Parameters that affect physics
    fd.density = 0.1;
    fd.friction = 0.1;
    fd.restitution = 0.5;

    // Attach Fixture to Body               
    _body.createFixture(fd);  
  }

  void setLinearVelocity(Vec2 vel) {
    _body.setLinearVelocity(vel); 
  }
  
  void setAngularVelocity(float vel) {
    _body.setAngularVelocity(vel);
  }
      
  void killBody() {
    _box2d.destroyBody(_body); 
  }
      
  Boolean destroyIfDone() {
    // Find the screen position of the particle
    Vec2 pos = _box2d.getBodyPixelCoord(_body);  
    // Is it off the bottom of the screen?
    if ((pos.x < -(PimmelSize / 2)) || (pos.x > (width + (PimmelSize / 2))) || (pos.y > height + (PimmelSize / 2))) {
      killBody();
      return true;
    }
    return false;
  }
      
  PShape createPimmelShape() {
    PShape s = createShape();
    s.beginShape(QUAD);
    s.noStroke();
    s.texture(_pimmelTexture);
    s.normal(0, 0, 1);
    // Let quad be a few pixels larger than the simulated circular body
    // to not cut off character features (e. g. in case of 'Q')
    float fHalfSize = PimmelSize / 2.0;
    s.vertex(-fHalfSize, -fHalfSize, 0, 0);
    s.vertex(fHalfSize, -fHalfSize, _pimmelTexture.width - 1, 0);
    s.vertex(fHalfSize, fHalfSize, _pimmelTexture.width - 1, _pimmelTexture.width - 1);
    s.vertex(-fHalfSize, fHalfSize, 0, _pimmelTexture.width - 1);
    s.endShape();
    return s;
  }
    
  void draw() {
    Vec2 pos = _box2d.getBodyPixelCoord(_body);
    float a = _body.getAngle();
    _s.resetMatrix();
    _s.rotate(-a);
    _s.translate(pos.x, pos.y);
    shape(_s);
  }  
}